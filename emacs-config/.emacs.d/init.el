;; package --- Summary
;; Commentary: Minhas configs do emacs
;; Configs Básicas

(setq inhibit-startup-message t) ;; remove welcome screen

(tool-bar-mode -1) ;; remove menu bar
(menu-bar-mode -1) ;; remove tool bar
(scroll-bar-mode -1) ;; remove roll bar
(global-display-line-numbers-mode t) ;; number of lines
(global-visual-line-mode t) ;; Broke line
(delete-selection-mode t) ;; Selected text are changed
(global-hl-line-mode -1) ;; Show line  brighter


(setq inhibit-startup-message t                    ; no welcome buffer
        visible-bell t                               ; blinks at cursor limits
        history-length 20                            ; max history saves
        use-dialog-box nil                           ; no ugly dialogs
        global-auto-revert-non-file-buffers t        ; update buffers thar are non-files too
        tab-always-indent 'complete                  ; use TAB to complete symbols
        mouse-wheel-scroll-amount '(2 ((shift) . 1)) ; scroll 2 lines
        mouse-wheel-progressive-speed nil            ; don't accelerate
        mouse-wheel-follow-mouse 't                  ; scroll window under mouse cursor
        scroll-step 1)                               ; scroll 1 line with keyboard
					
;; Tamanho da fonte

(set-face-attribute 'default nil :height 150)
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(font-lock-comment-face ((t (:slant italic)))))

;; Disable line numbers for some modes
(dolist (mode '(org-mode-hook
                term-mode-hook
                shell-mode-hook
	        treemacs-mode-hook
                eshell-mode-hook))
  (add-hook mode (lambda () (display-line-numbers-mode 0))))



;; ===========================================================
;; Pacotes
(require 'package)

(setq package-archives '(("gnu" . "http://elpa.gnu.org/packages/")
                         ("melpa" . "https://melpa.org/packages/")
                         ("nongnu" . "https://elpa.nongnu.org/nongnu/")
			 ("melpa-stable" . "http://stable.melpa.org/packages/")
			 ))


(prefer-coding-system       'utf-8)
(set-default-coding-systems 'utf-8)
(set-terminal-coding-system 'utf-8)
(set-keyboard-coding-system 'utf-8)
(setq default-buffer-file-coding-system 'utf-8)

;; ===========================================================

;; Pacotes inicialização e para melhor trabalho
(package-initialize) ; inicia os pacotes

;; Install use-package if not already installed
(unless (package-installed-p 'use-package)
  (package-refresh-contents)
  (package-install 'use-package))

;; Enable defer and ensure by default for use-package
(setq use-package-always-defer t
      use-package-always-ensure t)



;; command-log-mode
;;(use-package command-log-mode)
;; Try
(use-package try
  :ensure t)

;; Auto complete
(use-package auto-complete
  :ensure t
  :init
  (progn
    (ac-config-default)
    (global-auto-complete-mode t)
    )
  )


;; Wich key
(use-package which-key
  :config
  (setq which-key-idle-delay 0.25
        which-key-idle-secondary-delay 0.05
        which-key-show-remaining-keys nil)
  :init (which-key-mode)
  :bind ("C-c c w" . which-key-show-major-mode))



;; All the icons

(use-package all-the-icons
  :if (display-graphic-p)
  :ensure t
  )


;; Treemacs

;; (use-package treemacs
;;     :ensure t
;;     :bind
;;     (:map global-map
;;           ("M-\\" . treemacs))
;;     :config
;;     (setq treemacs-no-png-images t
;;           treemacs-is-never-other-window nil))


;; Neotree

(use-package neotree
  :ensure t
  :init
  (progn
    (global-set-key [f7] 'neotree-toggle)
    (setq neo-theme (if (display-graphic-p) 'icons 'arrow))
  )
  )


;; Rainbow Delimiters
(use-package rainbow-delimiters
  :ensure t
  :hook (prog-mode . rainbow-delimiters-mode))

;; transitar entre janelas com ace window

(use-package ace-window
  :ensure t
  :config
  (progn
   (global-set-key (kbd "M-o") 'ace-window))
  )


;; Ergoemacs
;;(use-package ergoemacs-mode
;;  :ensure t
;;  :config
;;  (progn
;;    (setq ergoemacs-theme nil) ;; Uses Standard Ergoemacs keyboard theme
;;    (setq ergoemacs-keyboard-layout "us") ;; Assumes QWERTY keyboard layout
;;    (setq ns-command-modifier 'meta) ;; modify my modifiers to work differently on a mac
;;    (setq ns-alternate-modifier 'super);; Command as the Alt key and Option as the Super key
;;    (ergoemacs-mode 0)
;;    )
;;  )


;; ===========================================================

;; temas
(add-to-list 'custom-theme-load-path "~/.emacs.d/themes/")

;; mistetioso
;;(load-theme 'misterioso)

;; molokai
;;(use-package molokai-theme
;;  :ensure t
;;  :config (load-theme 'molokai t))

;; zenburn
;;(load "~/.emacs.d/themes/zenburn-theme.el")
;;(use-package zenburn-theme
;;  :ensure t
;;  :config (load-theme 'zenburn t))

;; monokai
;;(use-package monokai-theme
;;  :ensure t
;;  :config (load-theme 'monokai))

(load "~/.emacs.d/themes/monokai-theme.el")
(load-theme 'monokai t)





;; ===========================================================

;; Eglot to configure programming languages
;; Docker
;; Terraform
;; Kubernetes
;; Rust
;; GO
;; Java/Scala
;; Python

;; ===========================================================
;; Initialize packages
(require 'package)
(add-to-list 'package-archives '("melpa" . "https://melpa.org/packages/") t)
(package-initialize)

;; Install eglot, company, and related packages if they are not already installed
(unless (package-installed-p 'eglot)
  (package-refresh-contents)
  (package-install 'eglot))

(unless (package-installed-p 'company)
  (package-refresh-contents)
  (package-install 'company))

(unless (package-installed-p 'flycheck)
  (package-refresh-contents)
  (package-install 'flycheck))

(unless (package-installed-p 'lsp-mode)
  (package-refresh-contents)
  (package-install 'lsp-mode))

(unless (package-installed-p 'lsp-ui)
  (package-refresh-contents)
  (package-install 'lsp-ui))

;; Enable scala-mode and sbt-mode
(use-package scala-mode
  :interpreter ("scala" . scala-mode))

;; Enable sbt mode for executing sbt commands
(use-package sbt-mode
  :commands sbt-start sbt-command
  :config
  ;; WORKAROUND: https://github.com/ensime/emacs-sbt-mode/issues/31
  ;; allows using SPACE when in the minibuffer
  (substitute-key-definition
   'minibuffer-complete-word
   'self-insert-command
   minibuffer-local-completion-map)
   ;; sbt-supershell kills sbt-mode:  https://github.com/hvesalai/emacs-sbt-mode/issues/152
   (setq sbt:program-options '("-Dsbt.supershell=false")))

;; Enable eglot and company
(use-package eglot
  :pin melpa-stable
  :config
  (add-to-list 'eglot-server-programs '(rust-mode . ("rls")))
  (add-to-list 'eglot-server-programs '(python-mode . ("pyls")))
  (add-to-list 'eglot-server-programs '(go-mode . ("gopls")))
  (add-to-list 'eglot-server-programs '(kubernetes-mode . ("kube-language-server")))
  (add-to-list 'eglot-server-programs '(dockerfile-mode . ("docker-langserver")))
  (add-to-list 'eglot-server-programs '(markdown-mode . ("markdown-language-server")))
  (add-to-list 'eglot-server-programs '((scala-mode . ("metals-emacs"))))
  :hook ((python-mode . eglot-ensure)
         (rust-mode . eglot-ensure)
         (go-mode . eglot-ensure)
         (kubernetes-mode . eglot-ensure)
         (dockerfile-mode . eglot-ensure)
         (markdown-mode . eglot-ensure)
         (scala-mode . eglot-ensure)))

(use-package company
  :config
  (global-company-mode))

;; Enable a linter, such as flake8 for Python or clippy for Rust
(use-package flycheck
  :ensure t
  :init (global-flycheck-mode t))

;; Enable autocomplete
(use-package auto-complete
  :config
  (ac-config-default))

;; Enable terminal integration
;; Configure terminal
(use-package multi-term
  :config
  (setq multi-term-program "/bin/bash"))


;; Enable eglot for all programming modes
(add-hook 'prog-mode-hook 'eglot-ensure)


;; Configure lsp-ui
(require 'lsp-ui)
(add-hook 'lsp-mode-hook 'lsp-ui-mode)

;; Configure terminal
(require 'multi-term)
(global-set-key (kbd "C-c t") 'multi-term)


;; Finalize configuration
(custom-set-variables
 '(eglot-autoshutdown t)
 '(lsp-ui-sideline-enable nil)
 '(lsp-ui-doc-enable nil))

(custom-set-faces)

;; Terraform
(unless (package-installed-p 'terraform-mode)
  (package-refresh-contents)
  (package-install 'terraform-mode))

;; https://github.com/emacsorphanage/terraform-mode
(require 'terraform-mode)
(add-to-list 'auto-mode-alist '("\\.tf\\'" . terraform-mode))
(add-hook 'terraform-mode-hook #'terraform-format-on-save-mode)

;; YAML
(unless (package-installed-p 'yaml-mode)
  (package-refresh-contents)
  (package-install 'yaml-mode)
(require 'yaml-mode)
(add-to-list 'auto-mode-alist '("\\.ya?ml\\'" . yaml-mode))


;; ===========================================================
;; coisas do melpa


;;(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
;; '(package-selected-packages
;;   '(eglot treemacs flycheck zenburn-theme which-key use-package try rainbow-delimiters neotree monokai-theme haskell-mode ergoemacs-mode auto-complete all-the-icons ace-window)))


;; Backups
(setq backup-directory-alist `(("." . "~/.saves")))

