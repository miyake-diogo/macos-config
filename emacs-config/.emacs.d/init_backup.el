;; Remover boas vindas
(setq inhibit-startup-message t)

;; Remover menus

(tool-bar-mode -1)
(menu-bar-mode -1)

;; Remover barra de rolagem
(scroll-bar-mode -1)

;; Numeros nas linhas
(global-linum-mode t)


;; Tamanho da fonte
(set-face-attribute 'default nil :height 150) 


;; Pacotes
(require 'package)
(setq package-enable-at-startup nil); desabilitar inicio de ativação de

; Melpa - repositorio
(add-to-list 'package-archives
             '("melpa-stable" . "https://stable.melpa.org/packages/") t) 


(package-initialize) ; inicia os pacotes

(unless (package-installed-p 'use-package)
  (package-refresh-contents)
  (package-install 'use-package))



;; Try 
(use-package try
  :ensure t)

;; Which key
(use-package which-key
  :ensure t
  :config (which-key-mode))

;; Auto complete
(use-package auto-complete
  :ensure t
  :init
  (progn
    (ac-config-default)
    (global-auto-complete-mode t)
    )
  )

;; All the icons

(use-package all-the-icons
  :if (display-graphic-p)
  :ensure t
  )


;; Neotree

(use-package neotree
  :ensure t
  :init
  (progn
    (global-set-key [f8] 'neotree-toggle)
    ()
  )
  )

(setq neo-theme (if (display-graphic-p) 'icons 'arrow))


;; coisas do melpa
(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(package-selected-packages '(neotree Neotree auto-complete wich-key try use-package)))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )

