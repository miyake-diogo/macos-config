# My MacOs Configuration files

To turn my life better when configure a new mac I create this repo to centralize my configs.  

Feel free to copy , clone or fork it.

# Apps

* Brew
* Iterm2
* ZSH - Ohmyzsh

# IDE's 
* Vscode
* Emacs

# Programming Languages
* Rust
* GO
* Scala
* Python
* Elixir

