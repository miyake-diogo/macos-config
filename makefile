LIMA_CONFIG_FOLDER:="$(pwd)/lima-config"

install-base:
	echo "installing homebrew + iterm2 and Ohmyzsh"
	/bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
	brew install --cask iterm2
	brew install zsh

install-emacs:
	echo "installing emacs and configuring it!!"
	brew update
  	brew install emacs
	cp emacs-config/init.el ~/emacs.d/init.el

install-vscode:
	echo "installing vs code by brew formulae"
	brew install --cask visual-studio-code

config-rust:
	echo "installing and configuring Rust!!"
	curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | sh
	echo "installing Rust Analyzer!!"
	git clone https://github.com/rust-analyzer/rust-analyzer.git
	cd rust-analyzer
	cargo xtask install --server # will install rust-analyzer into $HOME/.cargo/bin
	rm -rf rust-analyzer

config-go:
	echo "installing and configuring Go"
	brew install go
	echo 'export GOPATH=$(go env GOPATH)' >> ~/.bash_profile
	echo 'export PATH=$PATH:$GOPATH/bin' >> ~/.bash_profile
	source ~/.bash_profile

config-scala:
	echo "installing and configuring Scala"
	brew install scala

config-python:
	echo "installing and configuring python"
	brew install python

config-cloud-cli:
	echo "installing and configuring Cloud Cli's Azure, GCP and AWS"
	# Azure
	brew update && brew install azure-cli
	# AWS
	curl "https://awscli.amazonaws.com/AWSCLIV2.pkg" -o "AWSCLIV2.pkg"
	sudo installer -pkg AWSCLIV2.pkg -target /
	curl "https://awscli.amazonaws.com/AWSCLIV2.pkg" -o "AWSCLIV2.pkg"
	sudo installer -pkg ./AWSCLIV2.pkg -target /
	# GCP
	brew install --cask google-cloud-sdk

config-lima-amd:
	echo "installing and configuring lima with amd qemu emulation"
	limactl start --name=lima_docker_arm $(LIMA_CONFIG_FOLDER)/ubuntu_all_amd.yml 

config-lima:
	echo "installing and configuring lima"
	brew install lima
	limactl start --name=lima_docker_arm $(LIMA_CONFIG_FOLDER)/ubuntu_all_arm.yml 

config-terraform: 
	echo "installing and configuring Terraform"
	brew tap hashicorp/tap
	brew install hashicorp/tap/terraform

config-lunarvim:
	brew install node
	brew install neovim
	LV_BRANCH='release-1.2/neovim-0.8' bash <(curl -s https://raw.githubusercontent.com/lunarvim/lunarvim/fc6873809934917b470bff1b072171879899a36b/utils/installer/install.sh)
